# 3D Raycaster

Basically a glorified raycaster thats actual 3d written in js using canvas context 2d and not webgl


# Running

Hosting it locally (or statically, ig?) is the easiest. I use the VS Code Live Server Extention. I also have the Firefox Live Sever addon so the page automatically refreshes.


# Controls

Mouse to look

WASD to move on the 2D plane in the direction you're facing

Q to move down

E to move up

Left Click to remove the voxel infront of you

Right Click to place a voxel (set distance of 2 infront of you, had some issues with direction. works just fine though)

Scroll Up/Down to change selected voxel

+/- increase/decrase max number of reflections of reflective voxels

`]`/`[` increase/decrease view distance


# Voxel List

-1 - reflective voxel

0 - air. acts like a left click

1 - Uvmap

2 - DarkViperAU Wide

3 - Cirno Fumo

Anything Else - Error


# Background

I made a 2D raycaster for fun and then decided to make it 3D.[I did do this a few years ago](https://codepen.io/notwillair/pen/dyovGGM) but it was much worse XP.


# Discussion

Feel free to DM me on Discord at Givo#6140