let canvas = document.getElementById("2D-canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
let ctx = canvas.getContext("2d");
let lastUpdate = Date.now();

let mouse = 0;
let mouse2 = 0;

function loop() {
  let now = Date.now();
  let dT = now - lastUpdate;
  lastUpdate = now;

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  ctx.fillStyle = "#ff00ff";
  ctx.save();
  ctx.translate(canvas.width / 2, canvas.height / 2- 50);
  ctx.transform(mouse2 / 100, mouse / 100, 0, 1, 0, 0);
  ctx.fillRect(0, 0, 100, 100);
  ctx.restore();

  window.requestAnimationFrame(loop);
}

window.requestAnimationFrame(loop);

document.addEventListener("mousemove", e => {
  mouse = e.offsetY - canvas.height / 2;
  mouse2 = e.offsetX - canvas.width / 2;
});