let canvas = document.getElementById("2D-canvas");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
let ctx = canvas.getContext("2d");
let canvas2 = document.getElementById("2D-text-canvas");
canvas2.width = window.innerWidth;
canvas2.height = window.innerHeight;
let ctx2 = canvas2.getContext("2d");
let lastUpdate = Date.now();

let frameTimeCounter = 0;
let frameCounter = 0;
let pixelIncreased = 0;
let ditherOffset = 0;

async function LoadPage() {
  let pixelFont = new FontFace('Pixel Font', 'url("04B_19__.TTF")');
  await pixelFont.load();
  document.fonts.add(pixelFont);
  font = "Pixel Font";

  window.requestAnimationFrame(loop);
}

LoadPage();

let wallImages = [
  new Image(1000, 1000),
  new Image(1000, 1000),
  new Image(1000, 1000),
];

wallImages[0].src = "uvmap.png";
wallImages[1].src = "dviperWide.jpg";
wallImages[2].src = "fumo.png";

let grid = [];

let cellSize = 50;
let viewDistance = 15;
let fov = 90;
let pixelSize = 16;
let maxBounces = 0;
let block = 0;
let highlightBlock = {
  x: 0,
  y: 0,
  z: 0,
};

let player = {
  x: -0.5,
  y: 0.5,
  z: 0.5,
  xa: 0,
  ya: 0
};

let moveVec = {
  mf: 0,
  mb: 0,
  ml: 0,
  mr: 0,
  mu: 0,
  md: 0
};

function castRay(origin, ignoreHighlight, debug) {
  let rayX = origin.x;
  let rayY = origin.y;
  let rayZ = origin.z;
  let cos = Math.cos(origin.xa);
  let sin = Math.sin(origin.xa);
  let cos2 = Math.cos(origin.ya);
  let sin2 = Math.sin(origin.ya);
  let xDir = cos > 0 ? 0 : -1;
  let yDir = sin2 > 0 ? 0 : -1;
  let zDir = sin > 0 ? 0 : -1;
  let xScale = Math.abs(1 - (Math.abs(player.xa) % Math.PI) / (Math.PI / 2));
  let yScale = Math.abs(1 - (Math.abs(player.xa - Math.PI / 2) % Math.PI) / (Math.PI / 2));
  let cosMod = cos * (Math.abs(cos) > Math.PI / 4 ? cos2 : 1);
  let sinMod = sin * (Math.abs(sin) > Math.PI / 4 ? cos2 : 1);

  let castDistance = 0;
  while (castDistance < viewDistance) {
    castDistance++;

    let xRay = stepRay(cosMod, sinMod, sin2, rayX, rayZ, rayY);
    let zRay = stepRay(sinMod, cosMod, sin2, rayZ, rayX, rayY);
    let yRay = stepRay(sin2, cosMod, sinMod, rayY, rayX, rayZ);

    if (xRay.dist < zRay.dist && xRay.dist < yRay.dist) {
      rayX += xRay.val1;
      rayZ += xRay.val2;
      rayY += xRay.val3;

      let hit = getGridorUndefined(Math.floor(rayX + xDir), Math.floor(rayY), Math.floor(rayZ));
      if (hit != undefined && hit != 0) {
        return { dist: Math.hypot(Math.hypot(rayX - origin.x, rayY - origin.y), rayZ - origin.z), x: rayX + xDir, y: rayY, z: rayZ, t: hit - 1, tx: Math.abs(Math.abs(xDir) - (rayZ - Math.floor(rayZ))), ty: rayY - Math.floor(rayY), td: xDir, bd: { x: xDir, y: -2, z: -2 }, rb: { x: xDir, y: 0, z: 0 }, r: { x: function (a) { return -a + Math.PI }, y: function (a) { return a } } };
      } else if (!ignoreHighlight && Math.floor(rayX + xDir) == highlightBlock.x && Math.floor(rayY) == highlightBlock.y && Math.floor(rayZ) == highlightBlock.z) {
        return { dist: Math.hypot(Math.hypot(rayX - origin.x, rayY - origin.y), rayZ - origin.z), x: rayX + xDir, y: rayY, z: rayZ, t: -1, tx: Math.abs(Math.abs(xDir) - (rayZ - Math.floor(rayZ))), ty: rayY - Math.floor(rayY), td: xDir, bd: { x: xDir, y: -2, z: -2 }, rb: { x: xDir, y: 0, z: 0 }, r: { x: function (a) { return -a + Math.PI }, y: function (a) { return a } } };
      }
    } else if (zRay.dist < yRay.dist) {
      rayZ += zRay.val1;
      rayX += zRay.val2;
      rayY += zRay.val3;

      let hit = getGridorUndefined(Math.floor(rayX), Math.floor(rayY), Math.floor(rayZ + zDir));
      if (hit != undefined && hit != 0) {
        return { dist: Math.hypot(Math.hypot(rayX - origin.x, rayY - origin.y), rayZ - origin.z), x: rayX, y: rayY, z: rayZ + zDir, t: hit - 1, tx: Math.abs(Math.abs(1 + zDir) - (rayX - Math.floor(rayX))), ty: rayY - Math.floor(rayY), td: zDir, bd: { x: -2, y: -2, z: zDir }, rb: { x: 0, y: 0, z: zDir }, r: { x: function (a) { return -(a - Math.PI / 2) + Math.PI / 2 * 3 }, y: function (a) { return a } } };
      } else if (!ignoreHighlight && Math.floor(rayX) == highlightBlock.x && Math.floor(rayY) == highlightBlock.y && Math.floor(rayZ + zDir) == highlightBlock.z) {
        return { dist: Math.hypot(Math.hypot(rayX - origin.x, rayY - origin.y), rayZ - origin.z), x: rayX, y: rayY, z: rayZ + zDir, t: -1, tx: Math.abs(Math.abs(1 + zDir) - (rayX - Math.floor(rayX))), ty: rayY - Math.floor(rayY), td: zDir, bd: { x: -2, y: -2, z: zDir }, rb: { x: 0, y: 0, z: zDir }, r: { x: function (a) { return -(a - Math.PI / 2) + Math.PI / 2 * 3 }, y: function (a) { return a } } };
      }
    } else {
      rayY += yRay.val1;
      rayX += yRay.val2;
      rayZ += yRay.val3;

      let hit = getGridorUndefined(Math.floor(rayX), Math.floor(rayY + yDir), Math.floor(rayZ));
      if (hit != undefined && hit != 0) {
        return { dist: Math.hypot(Math.hypot(rayX - origin.x, rayY - origin.y), rayZ - origin.z), x: rayX, y: rayY + yDir, z: rayZ, t: hit - 1, tx: Math.abs(Math.abs(yDir) - (rayZ - Math.floor(rayZ))), ty: Math.abs(Math.abs(1 + yDir) - (rayX - Math.floor(rayX))), td: 0, bd: { x: -2, y: yDir, z: -2 }, rb: { x: 0, y: yDir, z: 0 }, r: { x: function (a) { return a }, y: function (a) { return -a } } };
      } else if (!ignoreHighlight && Math.floor(rayX) == highlightBlock.x && Math.floor(rayY + yDir) == highlightBlock.y && Math.floor(rayZ) == highlightBlock.z) {
        return { dist: Math.hypot(Math.hypot(rayX - origin.x, rayY - origin.y), rayZ - origin.z), x: rayX, y: rayY + yDir, z: rayZ, t: -1, tx: Math.abs(Math.abs(yDir) - (rayZ - Math.floor(rayZ))), ty: Math.abs(Math.abs(1 + yDir) - (rayX - Math.floor(rayX))), td: 0, bd: { x: -2, y: yDir, z: -2 }, rb: { x: 0, y: yDir, z: 0 }, r: { x: function (a) { return a }, y: function (a) { return -a } } };
      }
    }
  }
}

function stepRay(angle1, angle2, angle3, pos1, pos2, pos3) {
  let ray1 = angle1 > 0 ? Math.floor(pos1 + 1) - pos1 : Math.ceil(pos1 - 1) - pos1;
  let ray2 = ray1 * (angle2 / angle1);
  let ray3 = ray1 * (angle3 / angle1);

  return { dist: Math.hypot(Math.hypot(ray1, ray2), ray3), val1: ray1, val2: ray2, val3: ray3 };
}

function checkUndefinedGrid(x, y, z) {
  if (grid[y] == undefined || grid[y][x] == undefined || grid[y][x][z] == undefined) {
    return false;
  }

  return true;
}

function getGridorUndefined(x, y, z) {
  if (grid[y] == undefined || grid[y][x] == undefined || grid[y][x][z] == undefined) {
    return undefined;
  }

  return grid[y][x][z];
}

function placeBlock(x, y, z, b) {
  if (grid[y] == undefined) {
    grid[y] = [];
  }

  if (grid[y][x] == undefined) {
    grid[y][x] = [];
  }

  grid[y][x][z] = b;
}

function loop() {
  let now = Date.now();
  let dT = now - lastUpdate;
  lastUpdate = now;

  frameTimeCounter += dT;
  frameCounter++;

  if (frameCounter >= 300) {
    let averageFrames = frameTimeCounter / frameCounter;

    if (averageFrames > 17) {
      let offSet = averageFrames - 16;
      pixelSize += Math.round(offSet / 2);
    } else if (pixelIncreased != pixelSize) {
      let offSet = 16 - averageFrames;
      pixelIncreased = pixelSize;
      pixelSize--;
    }

    frameTimeCounter = 0;
    frameCounter = 0;
  }

  let rm = (moveVec.mf - moveVec.mb);
  let sm = (moveVec.mr - moveVec.ml);
  let vm = (moveVec.mu - moveVec.md);

  player.x += Math.cos(player.xa) * rm * 0.05 * (dT / 16);
  /*if (checkUndefinedGrid(Math.floor(player.x), Math.floor(player.y)) && grid[Math.floor(player.y)][Math.floor(player.x)] != 0) {
    player.x -= Math.cos(player.a) * rm * 0.05;
  }*/

  player.z += Math.sin(player.xa) * rm * 0.05 * (dT / 16);
  /*if (checkUndefinedGrid(Math.floor(player.x), Math.floor(player.y)) && grid[Math.floor(player.y)][Math.floor(player.x)] != 0) {
    player.y -= Math.sin(player.a) * rm * 0.05;
  }*/

  player.x += Math.sin(-player.xa) * sm * 0.05 * (dT / 16);
  /*if (checkUndefinedGrid(Math.floor(player.x), Math.floor(player.y)) && grid[Math.floor(player.y)][Math.floor(player.x)] != 0) {
    player.x -= Math.sin(-player.a) * sm * 0.05;
  }*/

  player.z += Math.cos(-player.xa) * sm * 0.05 * (dT / 16);
  /*if (checkUndefinedGrid(Math.floor(player.x), Math.floor(player.y)) && grid[Math.floor(player.y)][Math.floor(player.x)] != 0) {
    player.y -= Math.cos(-player.a) * sm * 0.05;
  }*/

  if (block != 0) {
    let highlight = castRay(player, true, false);

    if (highlight != null) {
      highlightBlock = {
        x: Math.floor(highlight.x) - (highlight.bd.x != -2 ? (highlight.bd.x == 0 ? 1 : -1) : 0),
        y: Math.floor(highlight.y) - (highlight.bd.y != -2 ? (highlight.bd.y == 0 ? 1 : -1) : 0),
        z: Math.floor(highlight.z) - (highlight.bd.z != -2 ? (highlight.bd.z == 0 ? 1 : -1) : 0)
      };
    } else {
      highlightBlock = {
        x: Math.floor(player.x + Math.cos(player.xa) * 3),
        y: Math.floor(player.y + Math.sin(player.ya) * 3),
        z: Math.floor(player.z + Math.sin(player.xa) * 3),
      };
    }
  } else {
    highlightBlock = {x: 100000000, y: 100000000, z: 100000000};
  }

  let xScale = Math.abs(1 - (Math.abs(player.xa) % Math.PI) / (Math.PI / 2));
  let yScale = Math.abs(1 - (Math.abs(player.xa - Math.PI / 2) % Math.PI) / (Math.PI / 2));

  player.y -= vm * 0.05;

  player.ya = Math.clamp(player.ya, -Math.PI / 2, Math.PI / 2);

  ctx.lineWidth = 2;

  ditherOffset = (ditherOffset + 1) % 2;

  for (let j = 0; j < canvas.height; j += pixelSize) {
    for (let i = pixelSize * ((ditherOffset + j) % 2);  i < canvas.width; i += (pixelSize * 2)) {
      let xAngle = player.xa - (fov / 2 * Math.PI / 180) + ((fov / canvas.width) * i * Math.PI / 180);
      let yFov = fov * (canvas.height / canvas.width);
      let yAngle = player.ya - (yFov / 2 * Math.PI / 180) + ((yFov / canvas.height) * j * Math.PI / 180);
      let hit = castRay({ x: player.x, y: player.y, z: player.z, xa: xAngle, ya: yAngle }, false, j == 0);

      let bounces = 0;
      let bounced = false;
      let prebounce = hit != null ? hit.t : 0;
      while (bounces < maxBounces && hit != null && hit.t == -2) {
        xAngle = hit.r.x(xAngle);
        yAngle = hit.r.y(yAngle);
        hit = castRay({ x: hit.x - hit.rb.x, y: hit.y - hit.rb.y, z: hit.z - hit.rb.z, xa: xAngle, ya: yAngle }, j == 0);
        bounces++;
        bounced = true;
      }

      if (prebounce == -2) {
        let color = 50;
        if (!bounced) color = (1 - (hit.dist / viewDistance)) * color;
        ctx.fillStyle = `hsl(25, 90%, ${color}%)`;
        ctx.fillRect(i, j, pixelSize, pixelSize);
        ctx.globalAlpha = bounced || hit.t == -1 ? 0.75 : 0;
      }

      if (yAngle < 0) {
        ctx.fillStyle = "hsl(195, 86%, 63%)";
        ctx.fillRect(i, j, pixelSize, pixelSize);
      } else if (yAngle >= 0) {
        ctx.fillStyle = "hsl(120, 63%, 61%)";
        ctx.fillRect(i, j, pixelSize, pixelSize);
      }

      if (hit != null && hit.t >= 0 && !bounced) {
        ctx.drawImage(wallImages[hit.t], hit.tx * 1000, hit.ty * 1000, pixelSize * hit.dist * (hit.td == 0 ? 1 : -1), pixelSize * hit.dist, i, j, pixelSize, pixelSize);
      } else if (hit != null && hit.t == -1 && !bounced) {
        ctx.globalAlpha = 0.25;
        ctx.drawImage(wallImages[0], hit.tx * 1000, hit.ty * 1000, pixelSize * hit.dist * (hit.td == 0 ? 1 : -1), pixelSize * hit.dist, i, j, pixelSize, pixelSize);
        ctx.globalAlpha = 1;
      } else if (hit != null && hit.t != -2) {
        ctx.save();
        ctx.translate(i, j);
        ctx.scale(bounces % 2 == 0 ? 1 : -1, 1);
        ctx.drawImage(wallImages[Math.abs(hit.t)], hit.tx * 1000, hit.ty * 1000, pixelSize * hit.dist * (hit.td == 0 ? 1 : -1), pixelSize * hit.dist, 0, 0, pixelSize, pixelSize);
        ctx.restore();
      }

      ctx.globalAlpha = 1;
    }
  }

  ctx2.clearRect(0, 0, canvas2.width, canvas2.height);
  ctx2.fillStyle = "#000000";
  ctx2.textAlign = "left";
  ctx2.font = "48px Pixel Font";
  ctx2.fillText(block, 5, canvas2.height - 5);

  ctx2.textAlign = "left";
  ctx2.fillText("FPS " + (1000 / dT).toFixed(2), 5, 48 + 5);
  ctx2.fillText("VIEW DISTANCE " + viewDistance, 5, (48 + 5) * 2);
  ctx2.fillText("PIXEL SIZE " + pixelSize, 5, (48 + 5) * 3);
  ctx2.fillText("MAX REFLECTIONS " + maxBounces, 5, (48 + 5) * 4);


  window.requestAnimationFrame(loop);
}

document.addEventListener("keydown", e => {
  if (!e.repeat) {
    switch (e.key) {
      case "W":
      case "w":
      case "ArrowUp": {
        moveVec.mf = 1;
      } break;

      case "S":
      case "s":
      case "ArrowDown": {
        moveVec.mb = 1;
      } break;

      case "A":
      case "a": {
        moveVec.ml = 1;
      } break;

      case "D":
      case "d": {
        moveVec.mr = 1;
      } break;

      case 'Q':
      case 'q': {
        moveVec.md = 1;
      } break;

      case 'E':
      case 'e': {
        moveVec.mu = 1;
      } break;

      case '+':
      case '=': {
        maxBounces++;
      } break;

      case '-':
      case '_': {
        maxBounces = maxBounces > 0 ? maxBounces - 1 : 0;
      } break;

      case ']':
      case '}': {
        viewDistance++;
      } break;

      case '[':
      case '{': {
        viewDistance = viewDistance > 1 ? viewDistance - 1 : 1;
      } break;
    }
  }
});

document.addEventListener("keyup", e => {
  switch (e.key) {
    case "W":
    case "w":
    case "ArrowUp": {
      moveVec.mf = 0;
    } break;

    case "S":
    case "s":
    case "ArrowDown": {
      moveVec.mb = 0;
    } break;

    case "A":
    case "a": {
      moveVec.ml = 0;
    } break;

    case "D":
    case "d": {
      moveVec.mr = 0;
    } break;

    case 'Q':
    case 'q': {
      moveVec.md = 0;
    } break;

    case 'E':
    case 'e': {
      moveVec.mu = 0;
    } break;
  }
});

document.addEventListener("mousemove", e => {
  player.xa += e.movementX / 100;
  player.ya += e.movementY / 100;
});

document.addEventListener("wheel", e => {
  if (e.deltaY < 0) {
    block--;
  } else if (e.deltaY > 0) {
    block++;
  }
});

document.addEventListener("mousedown", e => {
  let hit = castRay(player, true, false);
  let select = {
    x: Math.floor(player.x + Math.cos(player.xa) * 3),
    y: Math.floor(player.y + Math.sin(player.ya) * 3),
    z: Math.floor(player.z + Math.sin(player.xa) * 3),
  };

  switch (e.button) {
    case 0: {
      if (hit != null) {
        placeBlock(Math.floor(hit.x), Math.floor(hit.y), Math.floor(hit.z), 0);
      } else {
        placeBlock(select.x, select.y, select.z, 0);
      }
    } break;

    case 2: {
      if (hit != null) {
        let highlighted = {
          x: Math.floor(hit.x) - (hit.bd.x != -2 ? (hit.bd.x == 0 ? 1 : -1) : 0),
          y: Math.floor(hit.y) - (hit.bd.y != -2 ? (hit.bd.y == 0 ? 1 : -1) : 0),
          z: Math.floor(hit.z) - (hit.bd.z != -2 ? (hit.bd.z == 0 ? 1 : -1) : 0)
        };
        placeBlock(highlighted.x, highlighted.y, highlighted.z, block);
      } else {
        placeBlock(select.x, select.y, select.z, block);
      }
    } break;
  }
});

document.requestPointerLock = canvas.requestPointerLock ||
  canvas.mozRequestPointerLock;

document.exitPointerLock = document.exitPointerLock ||
  document.mozExitPointerLock;

document.onclick = function () {
  canvas.requestPointerLock();
};

Math.clamp = function (value, min, max) {
  return value < min ? min : value > max ? max : value;
};